package ru.kondratenko.tm;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        final Application application = new Application();
        final Task task = application.getTaskService().findByIndex(0);
        System.out.println(task);
        final Project project = application.getProjectService().findByIndex(0);
        System.out.println(project);
        application.getProjectTaskService().addTaskToProject(project.getId(), task.getId());
        System.out.println(application.getProjectTaskService().findAllByProjectId(project.getId()));
        application.getProjectTaskService().removeTaskFromProject(project.getId(),task.getId());
        System.out.println(application.getProjectTaskService().findAllByProjectId(project.getId()));
        assertTrue(true);
    }

}
