package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private List<User> users = new ArrayList<>();

    public User create(
            final String login, final String password,
            final String firstName, final String lastName, final Role role) {
        User user = findByLogin(login);
        if (user == null) {
            user = new User(login, password, firstName, lastName, role);
            users.add(user);
        }
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        User user = findByLogin(login);
        if (user == null) {
            user = new User(login, password, role);
            users.add(user);
        }
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User findById(final Long id) {
        for (final User user : users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User updateByLogin(final String login, final String password, final String firstName, final String lastName) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User updatePasswordByLogin(final String login, final String password) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User updatePasswordById(final Long id, final String password) {
        User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User updateById(final Long id, final String password, final String firstName, final String lastName) {
        User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User updateRoleById(final Long id, final Role role) {
        User user = findById(id);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User updateRoleByLogin(final String login, final Role role) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User removeByLogin(final String login) {
        User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User removeById(final Long id) {
        User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

}
